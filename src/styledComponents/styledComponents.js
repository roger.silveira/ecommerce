import styled, { css } from 'styled-components';

const Corpo = styled.body`
  background-color: #c6f7f7;
`;

const TituloSite = styled.h1`
  font:bold;
`;

const Titulo = styled.h2`
  margin: 20px 0;
`;

const Campo = styled.div`
  margin-top: 20px;
`;

const Rotulo = styled.label`
  width: 100px;
  font-size: 1.5em;

  @media ( max-width: 426px ) {
    display: block;
  }

  ${ props => props.largura50 && css`
    width: 50px;
  ` }

  ${ props => props.largura100 && css`
    width: 100px;
  ` }

  ${ props => props.largura150 && css`
    width: 150px;
  ` }
`;

const Input = styled.input`
  width: 300px;
  padding: 5px 10px;
  font-size: 1.25em;
  background-color: white;

  ${ props => props.fundoAmarelo && css`
    background-color: yellow;
  ` }
`;

const CaixaSelecao = styled.select`
  width: 300px;
  padding: 5px 10px;
  font-size: 1.25em;

  ${ props => props.largura50 && css`
    width: 50px;
  ` }

  ${ props => props.largura100 && css`
    width: 100px;
  ` }

  ${ props => props.largura150 && css`
    width: 150px;
  ` }
`;

const Botao = styled.button`
  margin-top: 20px;
`;

export { 
  Corpo,
  TituloSite,
  Titulo,
  Campo,
  Rotulo,
  Input,
  Botao,
  CaixaSelecao
};