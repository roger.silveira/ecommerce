import { configureStore } from '@reduxjs/toolkit';
import produtosReducer from './slices/produtosSlice';
import categoriasReducer from './slices/categoriasSlice';
import usuarioReducer from './slices/usuarioSlice';

export const store = configureStore({
  reducer: { // Reducer Global ou Maior
    produtos: produtosReducer,
    categorias: categoriasReducer,
    usuario: usuarioReducer,
  },
});
