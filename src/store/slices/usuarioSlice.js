import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  usuarioEstaLogado: false,
  usuarioLogado: null
};

export const usuarioSlice = createSlice({
  name: 'usuario',
  initialState,
  reducers: {
    salvarUsuario: (state, action) => {
      state.usuarioLogado = action.payload;
      state.usuarioEstaLogado = true;

      // return {
      //   ...state,
      //   usuarioLogado: action.payload,
      //   usuarioEstaLogado: true
      // };
    },

    apagarUsuario: state => state = initialState,
  },
});

export const { salvarUsuario, apagarUsuario } = usuarioSlice.actions;

export default usuarioSlice.reducer;
