import { createSlice } from '@reduxjs/toolkit';

const NENHUMA_CATEGORIA_SELECIONADA = 'NENHUMA_CATEGORIA_SELECIONADA';

const initialState = {
  lista: [],
  listaCompleta: [],
  categoriaSelecionada: NENHUMA_CATEGORIA_SELECIONADA,
  campoPesquisar: '',
  atualizarListaCompleta: true
};

export const produtosSlice = createSlice({
  name: 'produtos',
  initialState,
  reducers: {

    salvarProdutos: (state, action) => {
      const produtos = action.payload;
      state.lista = produtos;
      state.listaCompleta = produtos;
    },

    filtrarProdutosPorCategoria: (state, action) => {
      const categoriaSelecionada = action.payload;

      if( categoriaSelecionada !== NENHUMA_CATEGORIA_SELECIONADA ) {
        const produtosFiltrados = state.listaCompleta.filter( produto => produto.categoria === categoriaSelecionada );

        state.lista = produtosFiltrados;
        state.categoriaSelecionada = categoriaSelecionada;
      } else {
        state.lista = state.listaCompleta;
        state.categoriaSelecionada = NENHUMA_CATEGORIA_SELECIONADA;
      }
    },

    salvarCampoPesquisar: ( state, action ) => { 
      state.campoPesquisar = action.payload;
    },

    filtrarProdutosPeloCampoPesquisar: state => {

      const palavras = state.campoPesquisar.split(' ');

      const produtos = state.listaCompleta;

      const produtosFiltrados = produtos.filter( produto => {

        for( let i = 0; i < palavras.length; i++ ) {

          const palavra = palavras[i].toLowerCase();

          return produto.produto.toLowerCase().includes( palavra )
            || produto.descricao.toLowerCase().includes( palavra )
            || produto.categoria.toLowerCase().includes( palavra );

        }

      } );

      state.lista = produtosFiltrados;

    },

    atualizarListaCompletaProdutos: state => {
      state.atualizarListaCompleta = !state.atualizarListaCompleta;
    }

  },
});

export { NENHUMA_CATEGORIA_SELECIONADA };

export const { 
  salvarProdutos, 
  filtrarProdutosPorCategoria,
  salvarCampoPesquisar,
  filtrarProdutosPeloCampoPesquisar,
  atualizarListaCompletaProdutos
} = produtosSlice.actions;

export default produtosSlice.reducer;