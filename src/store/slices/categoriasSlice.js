import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  lista: [],
};

export const categoriasSlice = createSlice({
  name: 'categorias',
  initialState,
  reducers: {
    salvarCategorias: (state, action) => {
      const produtos = action.payload;

      const categorias = [];

      for( let i = 0; i < produtos.length; i++ ) {
        const produto = produtos[i];

        if( !categorias.includes( produto.categoria ) ) {
          categorias.push( produto.categoria );
        }
      }

      state.lista = categorias;
    },
  },
});

export const { salvarCategorias } = categoriasSlice.actions;

export default categoriasSlice.reducer;
