import React from 'react';

import './Rodape.css';

const Rodape = () => {

  return <>
    <footer className='p-3'>

      <span>Copyright &#169; - Infnet</span>

    </footer>
  </>;

};

export default Rodape;