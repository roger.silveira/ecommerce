import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';

import editarProdutoHelper from '../../utilitarios/editarProdutoHelper';

import { atualizarListaCompletaProdutos } from '../../store/slices/produtosSlice';

import pegarProdutoHelper from '../../utilitarios/pegarProdutoHelper';

const Edicao = () => {

  const dispatch = useDispatch();

  const history = useHistory();

  const { id } = useParams();

  const usuarioEstaLogado = useSelector( state => state.usuario.usuarioEstaLogado );
  const usuarioLogado = useSelector( state => state.usuario.usuarioLogado );
  const categorias = useSelector( state => state.categorias.lista );

  const [ titulo, setTitulo ] = useState( '' );
  const [ preco, setPreco ] = useState( '' );
  const [ descricao, setDescricao ] = useState( '' );
  const [ categoria, setCategoria ] = useState( '' );
  const [ imagem, setImagem ] = useState( '' );
  const [ imagemDestaque, setImagemDestaque ] = useState( '' );

  useEffect( async () => {

    const resultado = await pegarProdutoHelper( id );

    if( resultado.sucesso ) {
      const produto = resultado.produto;

      setTitulo( produto.produto );
      setPreco( produto.preco.replace( '.', ',' ) );
      setDescricao( produto.descricao );
      setCategoria( produto.categoria );
      setImagem( produto.imagem );
      setImagemDestaque( produto.imagem_destaque );
    } else {
      alert( 'Houve um erro.' );
    }

  }, [] );

  const editar = () => {

    const produtoEditado = {
      produto: titulo,
      preco: preco.replace( ',', '.' ),
      descricao: descricao,
      categoria: categoria,
      imagem: imagem,
      imagem_destaque: imagemDestaque
    };

    const resultado = editarProdutoHelper( id, produtoEditado );

    if( resultado ) {
      dispatch( atualizarListaCompletaProdutos() );
      history.push( '/' );
    } else {
      alert( 'Houve um erro.' );
    }

  };

  return <>

    { usuarioEstaLogado && usuarioLogado.tipo === '1' ?

      <>
        <h1>Tela de Edicao</h1>

        <form onSubmit={ e => e.preventDefault() }>

          <div className='cp-campo'>

            <label htmlFor='titulo'>Título: </label>
            <input 
              type='text' 
              id='titulo' 
              name='titulo' 
              onChange={ e => setTitulo( e.target.value ) }
              value={ titulo } />

          </div>

          <div className='cp-campo'>

            <label htmlFor='preco'>Preço: </label>
            <input 
              type='text'
              id='preco' 
              name='preco'
              onChange={ e => setPreco( e.target.value ) }
              value={ preco } />

          </div>

          <div className='cp-campo'>

            <label htmlFor='descricao'>Descrição: </label>
            <input 
              type='text' 
              id='descricao' 
              name='descricao'
              onChange={ e => setDescricao( e.target.value ) }
              value={ descricao } />

          </div>

          <div className='cp-campo'>

            <label htmlFor='categoria'>Categoria: </label>
            <select 
              id='categoria' 
              name='categoria'
              onChange={ e => setCategoria( e.target.value ) } >

                <option 
                  selected={ categoria === null }
                  disabled 
                  value='-1'>
                    Selecione aqui a categoria.
                </option>

                { categorias.map( item => <option 
                  value={ item }
                  selected={ item === categoria } >
                    { item }
                </option> ) }

            </select>

          </div>

          <div className='cp-campo'>

            <label htmlFor='imagem'>Imagem: </label>
            <input 
              type='text' 
              id='imagem' 
              name='imagem'
              onChange={ e => setImagem( e.target.value ) }
              value={ imagem } />

          </div>

          <div className='cp-campo'>

            <label htmlFor='imagem_destaque'>Imagem de Destaque: </label>
            <input 
              type='text' 
              id='imagem_destaque' 
              name='imagem_destaque'
              onChange={ e => setImagemDestaque( e.target.value ) }
              value={ imagemDestaque } />

          </div>

          <button 
            className='btn btn-primary'
            onClick={ () => editar() } >
              Editar
          </button>

        </form>
      </>

      :
      <p>Você não tem permissão para acessar esta tela.</p>
    } 
  </>;

};

export default Edicao;