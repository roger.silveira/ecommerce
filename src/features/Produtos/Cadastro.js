import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import salvarProdutoHelper from '../../utilitarios/salvarProdutoHelper';

import { atualizarListaCompletaProdutos } from '../../store/slices/produtosSlice';

import { 
  Titulo,
  Campo,
  Rotulo,
  Input,
  Botao,
  CaixaSelecao
 } from '../../styledComponents/styledComponents';

const Cadastro = () => {

  const dispatch = useDispatch();

  const history = useHistory();

  const usuarioEstaLogado = useSelector( state => state.usuario.usuarioEstaLogado );
  const usuarioLogado = useSelector( state => state.usuario.usuarioLogado );
  const categorias = useSelector( state => state.categorias.lista );

  const [ titulo, setTitulo ] = useState( '' );
  const [ preco, setPreco ] = useState( '' );
  const [ descricao, setDescricao ] = useState( '' );
  const [ categoria, setCategoria ] = useState( '' );
  const [ imagem, setImagem ] = useState( '' );
  const [ imagemDestaque, setImagemDestaque ] = useState( '' );

  const salvar = () => {

    const novoProduto = {
      produto: titulo,
      preco: preco.replace( ',', '.' ),
      descricao: descricao,
      categoria: categoria,
      imagem: imagem,
      imagem_destaque: imagemDestaque
    };

    const resultado = salvarProdutoHelper( novoProduto );

    if( resultado ) {
      dispatch( atualizarListaCompletaProdutos() );
      history.push( '/' );
    } else {
      alert( 'Houve um erro.' );
    }

  };

  return <>

    { usuarioEstaLogado && usuarioLogado.tipo === '1' ?

      <>
        <Titulo>Tela de Cadastro</Titulo>

        <form onSubmit={ e => e.preventDefault() }>

          <Campo>

            <Rotulo 
              htmlFor='titulo'
              largura150 >
                Título: 
            </Rotulo>
            <Input 
              type='text' 
              id='titulo' 
              name='titulo' 
              onChange={ e => setTitulo( e.target.value ) } />

          </Campo>

          <Campo>

            <Rotulo 
              htmlFor='preco'
              largura150 >
                Preço: 
            </Rotulo>
            <Input 
              type='text'
              id='preco' 
              name='preco'
              onChange={ e => setPreco( e.target.value ) } />

          </Campo>

          <Campo>

            <Rotulo 
              htmlFor='descricao'
              largura150 >
                Descrição: 
            </Rotulo>
            <Input 
              type='text' 
              id='descricao' 
              name='descricao'
              onChange={ e => setDescricao( e.target.value ) } />

          </Campo>

          <Campo>

            <Rotulo 
              htmlFor='categoria'
              largura150 >
                Categoria: 
            </Rotulo>
            <CaixaSelecao
              id='categoria' 
              name='categoria'
              onChange={ e => setCategoria( e.target.value ) } >

                <option 
                  selected
                  disabled 
                  value='-1'>
                    Selecione aqui a categoria.
                </option>

                { categorias.map( categoria => <option value={ categoria }>
                  { categoria }
                </option> ) }

            </CaixaSelecao>

          </Campo>

          <Campo>

            <Rotulo 
              htmlFor='imagem'
              largura150 >
                Imagem: 
            </Rotulo>
            <Input 
              type='text' 
              id='imagem' 
              name='imagem'
              onChange={ e => setImagem( e.target.value ) } />

          </Campo>

          <Campo>

            <Rotulo 
              htmlFor='imagem_destaque'
              largura150 >
                Imagem de Destaque: 
            </Rotulo>
            <Input 
              type='text' 
              id='imagem_destaque' 
              name='imagem_destaque'
              onChange={ e => setImagemDestaque( e.target.value ) } />

          </Campo>

          <Botao 
            className='btn btn-primary'
            onClick={ () => salvar() } >
              Salvar
          </Botao>

        </form>
      </>

      :
      <p>Você não tem permissão para acessar esta tela.</p>
    } 
  </>;

};

export default Cadastro;