import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Titulo } from '../../styledComponents/styledComponents';
import pegarProdutoHelper from '../../utilitarios/pegarProdutoHelper';

import './css/Detalhes.css';

const Detalhes = () => {

  const { id } = useParams();

  const [ produto, setProduto ] = useState( null );

  useEffect( async () => {

    const resultado = await pegarProdutoHelper( id );

    if( resultado.sucesso ) {
      setProduto( resultado.produto );
    } else {
      alert( 'Houve algum erro.' );
    }

  }, [] );

  return <>
  
    <Titulo>Detalhes do Produto: { produto?.produto }</Titulo>

    <div className="container">
      <div className="row">

        <div className="col">
          <img src={ produto?.imagem } />
        </div>

        <div className="col">
          
          <div id='dp-categoria'>
            Categoria: { produto?.categoria }
          </div>
          
          <div id='dp-descricao'>
            { produto?.descricao }
          </div>
          
          <div id='dp-preco'>
            R$ { produto?.preco }
          </div>
          
        </div>
      </div>
    </div>

  </>;

};

export default Detalhes;