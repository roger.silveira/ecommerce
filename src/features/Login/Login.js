import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import logarHelper from '../../utilitarios/logarHelper';

import { salvarUsuario } from '../../store/slices/usuarioSlice';

import { 
  Titulo,
  Campo,
  Rotulo,
  Input,
  Botao 
 } from '../../styledComponents/styledComponents';

const Login = () => {

  const dispatch = useDispatch();
  const history = useHistory();

  const [ usuario, setUsuario ] = useState('');
  const [ senha, setSenha ] = useState('');

  const logar = async () => {

    const resultado = await logarHelper( usuario, senha );

    if( resultado.sucesso ) {
      // Salvar no Redux as informações do usuário logado.
      dispatch( salvarUsuario( resultado.usuario ) );

      // Direciona o usuário para a tela inicial.
      history.push('/');
    } else { 
      alert( 'Usuário não autenticado.' );
    }

  };

  return <>
  
    <Titulo>Login</Titulo>

    <form onSubmit={ e => e.preventDefault() } >

      <Campo>
        <Rotulo htmlFor='usuario'>
          Usuário: 
        </Rotulo>
        <Input 
          type='text' 
          id='usuario' 
          name='usuario'
          onChange={ e => setUsuario( e.target.value ) } />
      </Campo>

      <Campo>
        <Rotulo htmlFor='senha'>
          Senha: 
        </Rotulo>
        <Input
          type='password' 
          id='senha' 
          name='senha'
          onChange={ e => setSenha( e.target.value ) }
          fundoAmarelo />
      </Campo>

      <Botao 
        className='btn btn-primary'
        onClick={ () => logar() } >
        Entrar
      </Botao>

    </form>

  </>;

};

export default Login;