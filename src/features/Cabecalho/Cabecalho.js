import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import MenuPrincipal from './MenuPrincipal/MenuPrincipal';

import './Cabecalho.css';

import { TituloSite } from '../../styledComponents/styledComponents';

const Cabecalho = () => {

  const usuarioEstaLogado = useSelector( state => state.usuario.usuarioEstaLogado );
  const usuarioLogado = useSelector( state => state.usuario.usuarioLogado );

  // const produtos = useSelector( state => state.produtos.lista );
  const produtos = useSelector( state => state.produtos.listaCompleta );

  const style = {
    'span': {
      fontWeight: 'bold'
    }
  };

  return <>
    <header>

      <div className="container p-3">
        <div className="row">

          <div className="col-12 col-sm-4 col-md-5">
            <Link to='/'>
              <img 
                src='/imagens/7d1f1391ef72ac61a5201452ccb91957.jpg'
                alt='Domino' 
                title='Logo do Domino.' />              
            </Link>
          </div>

          <div className="col-12 col-sm-4 col-md-5">
            <TituloSite>
              Dominó!
            </TituloSite>
          </div>

          <div className="col-12 col-sm-4 col-md-2">
            
            { usuarioEstaLogado ?
              <div>
                Seja bem-vindo, <span style={style.span}>{ usuarioLogado.nome }</span>.
              </div>
              : null }

            <div>
              { usuarioEstaLogado ?
                <Link to='logout'>
                  <i className="far fa-user"></i> Logout
                </Link>
                :
                <Link to='login'>
                  <i className="fas fa-user"></i> Login
                </Link>
              }
            </div>
          </div>

        </div>
      </div>

      <div className='container'>

        <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
          <div className="carousel-inner">

            { produtos.map( ( produto, indice ) => {

              const classeActive = indice === 0 ? 'active' : '';

              return <div 
                className={ `carousel-item ${ classeActive }` } 
                key={ produto.id } >
                  <img 
                    src={ produto.imagem_destaque } 
                    className="d-block" 
                    alt={ produto.produto } />
              </div>;
            } ) }

            {/* <div className="carousel-item active">
              <img src="..." className="d-block w-100" alt="..." />
            </div>
            <div className="carousel-item">
              <img src="..." className="d-block w-100" alt="..." />
            </div>
            <div className="carousel-item">
              <img src="..." className="d-block w-100" alt="..." />
            </div> */}

          </div>
          <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>

      </div>

      <MenuPrincipal />
    </header>
  </>;

};

export default Cabecalho;