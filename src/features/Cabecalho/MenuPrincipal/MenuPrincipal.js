import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import { 
  filtrarProdutosPorCategoria, 
  NENHUMA_CATEGORIA_SELECIONADA,
  salvarCampoPesquisar,
  filtrarProdutosPeloCampoPesquisar
} from '../../../store/slices/produtosSlice';

const MenuPrincipal = () => {

  const lista = useSelector( state => state.categorias.lista );
  const usuarioEstaLogado = useSelector( state => state.usuario.usuarioEstaLogado );
  const usuarioLogado = useSelector( state => state.usuario.usuarioLogado );

  const dispatch = useDispatch();

  return <>

    <div className='container'>

      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">

          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          
          <div className="collapse navbar-collapse" id="navbarSupportedContent">

            <ul className="navbar-nav me-auto mb-2 mb-lg-0">

              <li className="nav-item dropdown">

                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Categorias
                </a>

                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">

                  { lista.map( categoria => <li key={ categoria }>
                    <a 
                      className="dropdown-item" 
                      href="#"
                      onClick={ () => dispatch( filtrarProdutosPorCategoria( categoria ) ) }>
                      { categoria }
                    </a>
                  </li> ) }

                  <li key={ NENHUMA_CATEGORIA_SELECIONADA }>
                    <a 
                      className="dropdown-item" 
                      href="#"
                      onClick={ () => dispatch( filtrarProdutosPorCategoria( NENHUMA_CATEGORIA_SELECIONADA ) ) } >
                      Todos os Produtos
                    </a>
                  </li>
                </ul>
                
              </li>

              { usuarioEstaLogado && usuarioLogado.tipo === '1' ? 
                <li className="nav-item">
                  <Link 
                    to='cadastro-produto'
                    className="nav-link"
                    disable={ usuarioEstaLogado && usuarioLogado.tipo === '1' }
                    >
                      Cadastrar Produto
                  </Link>
                </li>
                : null }

            </ul>

            <form 
              className="d-flex"
              onSubmit={ e => e.preventDefault() }>
              <input 
                className="form-control me-2" 
                type="search" 
                placeholder="Pesquisar" 
                aria-label="Pesquisar"
                onChange={ e => dispatch( salvarCampoPesquisar( e.target.value ) ) } />
              <button 
                className="btn btn-outline-success" 
                type="submit"
                onClick={ e => dispatch( filtrarProdutosPeloCampoPesquisar() ) } >
                  Pesquisar
              </button>
            </form>

          </div>
        </div>
      </nav>

    </div>
  </>;

};

export default MenuPrincipal;