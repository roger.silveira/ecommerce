import axios from "axios";

const excluirProdutoHelper = async id => {

  const axios2 = axios.create({
    baseURL: 'http://localhost:8000/v2/',
    headers: { 
      token: JSON.parse(localStorage.getItem('token'))
    }
  });

  try {
    const respota = await axios2.delete( `cardapio/${ id }` );
    return true;
  } catch(e) {
    return false;
  }

};

export default excluirProdutoHelper;