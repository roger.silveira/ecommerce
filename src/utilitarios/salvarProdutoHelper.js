import axios from "axios";

const salvarProdutoHelper = async novoProduto => {

  const axios2 = axios.create({
    baseURL: 'http://localhost:8000/v2/',
    headers: { 
      token: JSON.parse(localStorage.getItem('token'))
    }
  });

  try {

    const resposta = await axios2.post( 'cardapio', novoProduto );
    return true;

  } catch(e) {

    return false;

  }

};

export default salvarProdutoHelper;