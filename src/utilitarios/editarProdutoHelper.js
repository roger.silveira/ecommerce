import axios from "axios";

const editarProdutoHelper = async ( id, produtoEditado ) => {

  const axios2 = axios.create({
    baseURL: 'http://localhost:8000/v2/',
    headers: { 
      token: JSON.parse(localStorage.getItem('token'))
    }
  });

  try {
    const resposta = axios2.put( `cardapio/${ id }`, produtoEditado );
    return true;
  } catch(e) {
    return false;
  }

};

export default editarProdutoHelper;