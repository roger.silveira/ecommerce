import axios from "../axios/axios";

const logarHelper = async ( usuario, senha ) => {

  try {
    const dados = { usuario, senha };
    const resultado = await axios.post( 'auth', dados );

    // Salvar o token no localStorage
    const token = resultado.data.token;
    localStorage.setItem( 'token', JSON.stringify(token) );

    return {
      sucesso: true,
      usuario: resultado.data.usuario
    };
  } catch (e) {

    return {
      sucesso: false,
      usuario: null
    };
    
  }

};

export default logarHelper;