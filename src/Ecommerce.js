import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Cabecalho from './features/Cabecalho/Cabecalho';
import Rodape from './features/Rodape/Rodape';
import BarraLateral from './features/BarraLateral/BarraLateral';
import ConteudoCentral from './features/ConteudoCentral/ConteudoCentral';
import Login from './features/Login/Login';
import Logout from './features/Logout/Logout';
import Cadastro from './features/Produtos/Cadastro';
import Edicao from './features/Produtos/Edicao';
import Detalhes from './features/Produtos/Detalhes';

import axios from './axios/axios';
import { salvarProdutos } from './store/slices/produtosSlice';
import { salvarCategorias } from './store/slices/categoriasSlice';

import { Corpo } from './styledComponents/styledComponents';

const Ecommerce = () => {

  const dispatch = useDispatch();

  const atualizarListaCompleta = useSelector( state => state.produtos.atualizarListaCompleta );

  useEffect( async () => {

    try{

      const resultado = await axios.get( 'cardapio' );
      dispatch( salvarProdutos( resultado.data ) );
      dispatch( salvarCategorias( resultado.data ) );

    } catch( e ) {
      alert('Houve algum problema na comunicação com a API.');
    }

  }, [atualizarListaCompleta] );

  return <>

    <BrowserRouter>

    <Corpo>
      <div id='ecommerce'>

        <Cabecalho />

        <div className="container">
          <Switch>

            <Route exact path='/'>
    
              <div className="row">

                {/* <div className="col-2">
                  <BarraLateral />
                </div> */}

                <div className="col-12">
                  <ConteudoCentral />
                </div>
                
              </div>
        
            </Route>

            <Route path='/login' component={ Login } />
            <Route path='/logout' component={ Logout } />
            <Route path='/cadastro-produto' component={ Cadastro } />
            <Route path='/edicao-produto/:id' component={ Edicao } />
            <Route path='/detalhes-produto/:id' component={ Detalhes } />

          </Switch>

        </div>

        <Rodape />

      </div>
    </Corpo>

    </BrowserRouter>

  </>;

};

export default Ecommerce;